package polly.flicdoorbell;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v7.app.NotificationCompat;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Date;

import io.flic.lib.FlicBroadcastReceiver;
import io.flic.lib.FlicButton;
import io.flic.lib.FlicManager;

/**
 * Created by polly on 24/07/2017.
 */

public class ExampleBroadcastReceiver extends FlicBroadcastReceiver {

    private static int notificationId = 0;

    @Override
    protected void onRequestAppCredentials(Context context) {
        // Set app credentials by calling FlicManager.setAppCredentials here
        FlicManager.setAppCredentials("baf81199-2238-4972-8efd-f34c33a03377", "36bdb808-d9a6-4be5-8b8a-e7ead009fad8", "Doorbell");

    }

    @Override
    public void onButtonUpOrDown(Context context, FlicButton button, boolean wasQueued, int timeDiff, boolean isUp, boolean isDown) {

        if (isDown) {
            sendNotification(context);
            Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
            final Ringtone ringtone = RingtoneManager.getRingtone(context.getApplicationContext(), notification);
            ringtone.play();
            new Runnable() {

                @Override
                public void run() {
                    try {
                        Thread.sleep(10000);
                        ringtone.stop();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }.run();
        }
    }

    private void sendNotification(Context context) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm 'on' dd MMM yyyy z");
        android.support.v7.app.NotificationCompat.Builder mBuilder =
                (android.support.v7.app.NotificationCompat.Builder) new android.support.v7.app.NotificationCompat.Builder(context)
                        .setSmallIcon(R.drawable.ic_stat_name)
                        .setContentTitle("Doorbell notification")
                        .setContentText("Doorbell rang at " + dateFormat.format(new Date()));


// Adds the Intent that starts the Activity to the top of the stack

        NotificationManager mNotificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

// mNotificationId is a unique integer your app uses to identify the
// notification. For example, to cancel the notification, you can pass its ID
// number to NotificationManager.cancel().

        try {
            mNotificationManager.notify(notificationId++, mBuilder.build());
        } catch (Exception e) {
            Toast.makeText(context, e.toString(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onButtonRemoved(Context context, FlicButton button) {
        // Button was removed
    }
}